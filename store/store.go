package store

import (
	"go_oauth2/constant"
	"go_oauth2/store/stores/memory_store"
	"go_oauth2/store/stores/redis_store"
)

type Store interface {
	Delete(token string) (err error)
	Find(token string) (b []byte, found bool, err error)
	Save(token string, b []byte) (err error)
}

var store Store

func NewStore(storeType string) {
	if storeType == constant.Memory {
		store = memory_store.New()
	} else if storeType == constant.Redis {
		store = redis_store.New(storeType)
	}
}

func GetStore() Store {
	return store
}
