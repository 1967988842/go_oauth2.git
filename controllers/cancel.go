package controllers

import (
	"github.com/kataras/iris"
	"go_oauth2/constant"
	"go_oauth2/store"
)

type CancelTokenRequestJson struct {
	GrantType string `json:"grant_type" validate:"required"`
}

func Cancel(context iris.Context) {
	// 校验请求头中的access_token是否存在
	accessToken := context.GetHeader(constant.Authorization)
	if len(accessToken) == 0 {
		// 未授权
		context.StatusCode(iris.StatusUnauthorized)
		// 未授权
		_, _ = context.JSON(ApiResourceError(constant.UserAuthorizationError))
		return
	}

	// 格式化输入信息
	cancelTokenRequestJson := new(CancelTokenRequestJson)
	if err := context.ReadJSON(&cancelTokenRequestJson); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能格式化成结构体
		_, _ = context.JSON(ApiResourceError(constant.RequestBodyError))
		return
	}

	// 校验结构体参数是否符合规则
	if err := validate.Struct(cancelTokenRequestJson); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 参数不符合规范
		_, _ = context.JSON(ApiResourceError(validatorErrorData(err)))
		return
	}

	// 判断grant_type是否符合规则
	if cancelTokenRequestJson.GrantType != constant.CancelToken {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 参数不符合规范
		_, _ = context.JSON(ApiResourceError(constant.RequestBodyError))
		return
	}

	session := store.GetStore()
	if err := session.Delete(accessToken); err != nil {
		// 程序内部错误
		context.StatusCode(iris.StatusInternalServerError)
		// 程序内部意外的错误
		_, _ = context.JSON(ApiResourceError(constant.ProgramError))
		return
	}

	// 已经清除授权，需要重新登录
	context.StatusCode(iris.StatusUnauthorized)
	// 需要重新登录
	_, _ = context.JSON(ApiResourceError(constant.AccessTokenTimeoutError))

}
