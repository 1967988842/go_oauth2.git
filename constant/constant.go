package constant

const (
	Salt                       = "go@im&oauth2"
	Redis                      = "oauth2.redis"
	Memory                     = "oauth2.memory"
	TimeFormat                 = "2006-01-02 15:04:05"
	TokenDefaultTimeout        = 60 * 60 * 2
	RefreshTokenDefaultTimeout = 60 * 60 * 24 * 7
	TokenDefaultType           = "bearer"
	Authorization              = "Authorization"
	Password                   = "password"
	RefreshToken               = "refresh_token"
	CheckToken                 = "check_token"
	CancelToken                = "cancel_token"
	ProgramError               = "程序内部意外的错误，请联系管理员！"
	ReadConfigError            = "读取配置文件错误！"
	RequestBodyError           = "请求参数错误！"
	AuthorizationError         = "Authorization不符合规范！"
	ClientAuthorizationError   = "客户端未授权！"
	UsernameOrPasswordError    = "账号或者密码错误！"
	UserAuthorizationError     = "用户未授权！"
	AccessTokenTimeoutError    = "access_token已经失效！请重新登录"
	RefreshTokenTimeoutError   = "refresh_token已经失效！请重新登录"
)
