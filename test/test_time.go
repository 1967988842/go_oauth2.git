package main

import (
	"fmt"
	"go_oauth2/constant"
	"go_oauth2/utils"
	"time"
)

func main() {
	expTime := time.Now().Unix() + constant.RefreshTokenDefaultTimeout
	t := time.Unix(expTime, 0)
	dateString := utils.CompareDateString(time.Now(), t)
	fmt.Println(dateString)
	date := t.Format("2006-01-02 15:04:05")
	fmt.Println(date)
}
