package utils

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/dgrijalva/jwt-go"
	"go_oauth2/config"
	"go_oauth2/constant"
	"time"
	"unicode"
)

func CompareInsensitive(a, b string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] == b[i] {
			continue
		}
		if unicode.ToLower(rune(a[i])) != unicode.ToLower(rune(b[i])) {
			return false
		}
	}
	return true
}

func CalcMD5(password string) string {
	m := md5.New()
	m.Write([]byte(password))
	m.Write([]byte(constant.Salt))
	return hex.EncodeToString(m.Sum(nil))
}

func CompareDateString(t1 time.Time, t2 time.Time) bool {
	return t1.After(t2)
}

func GetToken(clientId string, username string, expireTime string) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"clientId":   clientId,
		"username":   username,
		"expireTime": expireTime,
	})
	tokenString, _ := token.SignedString([]byte(config.Config.Get("oauth2.secret").(string)))
	return tokenString
}
