package models

import (
	"go_oauth2/database"
	"go_oauth2/utils"
	"time"
)

type UserInfo struct {
	ID        uint64    `gorm:"primary_key AUTO_INCREMENT COMMENT '主键，自动生成'"`
	Account   string    `gorm:"not null VARCHAR(50) COMMENT '用户账号'"`
	Password  string    `gorm:"not null VARCHAR(50) COMMENT '用户密码'"`
	Nickname  string    `gorm:"VARCHAR(500) COMMENT '用户昵称'"`
	Signature string    `gorm:"VARCHAR(50) COMMENT '用户签名'"`
	Mail      string    `gorm:"VARCHAR(50) COMMENT '用户邮箱'"`
	Mobile    string    `gorm:"VARCHAR(50) COMMENT '用户电话'"`
	AddTime   time.Time `gorm:"COMMENT '创建时间'"`
	EditTime  time.Time `gorm:"COMMENT '更新时间'"`
	DelFlag   uint32    `gorm:"COMMENT '删除标志位'"`
}

/**
 * 用户登录校验
 * @method UserCheckLogin
 * @param [string] username [用户账号]
 * @param [string] password [用户密码（加密）]
 */
func UserCheckLogin(username string, password string) bool {
	userInfo := UserInfo{}
	userInfo.Account = username
	if err := database.DB.First(&userInfo).Error; err != nil {
		return false
	}
	if utils.CompareInsensitive(userInfo.Password, utils.CalcMD5(password)) {
		return true
	}
	return false
}
