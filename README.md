# go_oauth2

#### 介绍
使用go iris实现sso服务（暂时只实现了password认证，而且客户端的sdk还没有编写）


#### 软件架构
1. web: github.com/kataras/iris
2. token: github.com/dgrijalva/jwt-go
3. toml: github.com/pelletier/go-toml
4. database: github.com/jinzhu/gorm


#### 安装教程
1. 下载项目
2. go mod download
3. go mod vendor
4. 运行main.go

#### 使用说明
1. go mod init go_oauth2
2. go build
3. go mod download
4. go mod vendor
5. go build -mod=vendor


#### 截图说明问题
查看附件页的示例视频